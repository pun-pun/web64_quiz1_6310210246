import { Link } from "react-router-dom";

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

function Header() {
    return(

    <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
            <Toolbar>
                <Typography variant="h7">
                    โปรแกรมตรวจสอบเลขคู่คี่ : 
                </Typography>

                &nbsp;&nbsp;
                <Link to="/">
                    <Typography variant="body1">
                        <Button color="info" variant="contained">เริ่มตรวจสอบ</Button>
                    </Typography>
                </Link>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <Link to="about">
                    <Typography variant="body1">
                        <Button color="info" variant="contained">เกี่ยวกับเรา</Button>
                    </Typography>
                </Link>

            </Toolbar>
        </AppBar>
    </Box>     
      
    );
}

export default Header; 