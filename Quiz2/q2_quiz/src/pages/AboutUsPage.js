import AboutUs from "../components/AboutUs";

function AboutUsPage() {
    return (
        <div>
            <div align="center">
                <h2> ผู้จัดทำ </h2>
                <AboutUs name="ปัณณทัต"
                         privatenum="6310210246" />

            </div>
        </div>
    );
}

export default AboutUsPage;