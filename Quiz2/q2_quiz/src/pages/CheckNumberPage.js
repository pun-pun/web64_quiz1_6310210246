import { useState } from "react";
import CheckNumber from "../components/CheckNumber";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Box, Container } from '@mui/material';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';

function CheckNumberPage() {

    const [ textmath, setTextmath ] = useState();
    const [ translatecheck, setTranslateCheck ] = useState("");

    function CheckAnswer() {

        if ( textmath != 0 ) {
            if ( (textmath%2) == 0) {
                setTranslateCheck("เลขคู่");
            }
            else if ( (textmath%2) == 1) {
                setTranslateCheck("เลขคี่");
            }
        }
    
        else if ( textmath == 0) {
            setTranslateCheck("เลขศูนย์");
        }
        
    }

    return(
        <Container maxWidth='ig'>
            <Grid container spacing={3} sx={{ marginTop : "10px" }}>
                <Grid item xs={6}>
                    <Box sx={{ textAlign : 'left'}}>
                        <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            กรอกตัวเลขที่ต้องการตรวจสอบ : <input type="text"
                                value={ textmath } 
                                onChange={ (e) => {setTextmath(e.target.value);} }/> <br />
                        < br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <Button color="secondary" variant="contained" onClick={ () => { CheckAnswer() }}>ตรวจสอบ</Button>
                    </Box>
                </Grid>
                <Grid item xs={4}>
                    <Card sx={{ minWidth: 275 }}>
                        <CardContent>
                            { textmath != '' &&
                                <CheckNumber
                                number = { textmath }
                                answer = { translatecheck }        
                                />
                            }
                                
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </Container>
        
    );

}

export default CheckNumberPage;