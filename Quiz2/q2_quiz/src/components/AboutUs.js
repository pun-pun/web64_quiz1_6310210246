import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (props) {

        return (
            <Box sx={{width:"22%"}}>
                <Paper elevation={2}>
                    <br />
                    <h2> จัดทำโดย : {props.name} </h2>
                    <h3> รหัส     : {props.privatenum} </h3>
                    <br />
                </Paper>
            </Box>
        );
    }
    
    export default AboutUs;