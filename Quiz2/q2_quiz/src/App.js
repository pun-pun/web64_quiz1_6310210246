import './App.css';

import Header from './components/Header';
import CheckNumberPage from './pages/CheckNumberPage';
import AboutUsPage from './pages/AboutUsPage';

import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element={
          <AboutUsPage />
          } />

          <Route path="/" element={
          <CheckNumberPage />
          } />

      </Routes>
    </div>
  );
}

export default App;
